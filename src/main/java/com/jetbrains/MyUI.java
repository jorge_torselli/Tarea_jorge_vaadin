package com.jetbrains;

import javax.servlet.annotation.WebServlet;

import clases.Cliente;
import clases.Clientes;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    private Clientes cl = new Clientes();


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();

        final TextField name = new TextField();
        name.setCaption("Nombre:");

        final TextField codigo = new TextField();
        codigo.setCaption("Codigo:");

        HorizontalLayout horizontalLayout = new HorizontalLayout();

        Button button = new Button("Ingresar datos");

        button.addClickListener(e -> {

            cl.setCliente(name.getValue(), Integer.parseInt(codigo.getValue()));
            name.setValue("");
            codigo.setValue("");
        });

        Button button1 = new Button("Ver datos");

        button1.addClickListener(e -> {
            VerticalLayout verticalLayout = new VerticalLayout();
            Grid<Cliente> grid = new Grid<>();
            grid.setItems(cl.listadoclientes);
            grid.addColumn(Cliente::getNombre).setCaption("Nombre");
            grid.addColumn(Cliente::getCodigo).setCaption(("Codigo"));
            verticalLayout.addComponent(grid);
            layout.addComponent(verticalLayout);
        });

        horizontalLayout.addComponents(button, button1);


        layout.addComponents(name, codigo, horizontalLayout);

        setContent(layout);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}

