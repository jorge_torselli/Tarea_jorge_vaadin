package clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by repre on 11/07/2017.
 */
public class Clientes {

    public List<Cliente> listadoclientes;

    public Clientes() {
        listadoclientes = new ArrayList<>();
    }

    public List<Cliente> getCliente() {
        return listadoclientes;
    }

    public void setCliente(String nom, int cod){

        Cliente cliente = new Cliente();
        cliente.setNombre(nom);
        cliente.setCodigo(cod);
        listadoclientes.add(cliente);
    }
}
