package clases;

/**
 * Created by repre on 11/07/2017.
 */
public class Cliente {
    private String nombre;
    private int codigo;

    public Cliente() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }


}
